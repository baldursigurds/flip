/* flip - a flashcard program
 * Copyright © 2017 Baldur Sigurðsson baldursigurds at gmail dot com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE_EXTENDED 
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <db.h>
#include <string.h>
#include <curses.h>
#include <locale.h>
#include <wchar.h>
#include <time.h>

#define MESSAGE_L 30
#define MESSAGE_H 5
#define MESSAGE_P MESSAGE_L*MESSAGE_H

#define EF_BASE 1.7
#define EF_MIN 1.3
#define EF_MAX 2.5
#define DAY 86400
#define VERSION 0.1

typedef struct card card;

struct card
{
    wchar_t front[MESSAGE_P+1];
    wchar_t back[MESSAGE_P+1];
    
    // time of next flip
    time_t date;

    // I is the interval
    int I;

    // EF is the easiness factor
    float EF;

    // auxiliary guy for the shuffling
    int sort;
};

typedef struct cardnode cardnode;

struct cardnode
{
    card C;
    // n is the identifier in the database
    int n;
    int sort;
    cardnode *next;
    cardnode *prev;
};



DB *init_db(char *location);
void quiz_loop(DB *deck);
char quiz_card(card *C);
void menu(DB* deck);
void add_card(DB* deck);
int max_key(DB* deck);
void clear_board();
void inputbox(int y, int x, int l, int h, wchar_t *s);
void shuffle_deck(cardnode **top);
void q_sort(cardnode **Top);
void b_sort(cardnode **Top);
void draw_frame();
void draw_art();
void edit_card(card * C);
void draw_side(int y, int x, wchar_t *side);
void replace_card(card * C, int n);
void delay_card(cardnode ** CN);
int count_cards(DB* deck);
int intlog10(int n);
//void clean_database();



// today is unix time of execution (in seconds).
time_t today;
// deck is a pointer to the database.
DB *deck;
char deck_loc[201];

int main(int argc, char * argv[])
{
    extern char *optarg;
    extern int optind, optop;
    
    // dbsrc indicates where we get the database from:
    // 0 is default, ~/.flip/flip.db
    // 1 is specified by commandline
    int dbsrc = 0;
    int do_cleaning = 0;
    char c;
    while( (c=getopt(argc, argv, "d:c")) != -1)
    {
        switch(c)
        {
            case 'd':
                strncpy(deck_loc, optarg, 200);
                dbsrc = 1;
                break;
            case 'c':
                do_cleaning = 1;
                break;
        }
    }

    // set the time
    today = time(NULL);

    // set the path to the database, unless it was set by argument
    if(dbsrc == 0)
    {
        strncpy(deck_loc, getenv("HOME"),200);
        strncat(deck_loc, "/.flip/flip.db", 200-strlen(deck_loc));
    }

    // Set up the database
    deck = init_db(deck_loc);

/*    // Clean it, if required (-c)
    if(do_cleaning)
    {
        clean_database();
        if(deck != NULL)
            deck->close(deck,0);
        return 0;
    }*/

    // set up the curses stuff
    setlocale(LC_ALL, "");
    initscr();
    clear();
    noecho();
    curs_set(0);
    start_color();
//    keypad(stdscr, TRUE);
    init_color(20, 300, 500, 0);
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_BLACK);
    init_pair(3, COLOR_YELLOW, COLOR_BLACK);
    init_pair(4, COLOR_BLUE, COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_CYAN, COLOR_BLACK);
    init_pair(7, COLOR_RED, COLOR_YELLOW);
    init_pair(8, COLOR_GREEN, COLOR_YELLOW);

    draw_frame();

    menu(deck);


    if(deck != NULL)
        deck->close(deck,0);

    endwin();

    return 0;
}

DB *init_db(char * location)
{
    DB *dbase;
    int ret;

    // Create the database structure
    if((ret = db_create(&dbase, NULL, 0)) != 0)
    {
        fprintf(stderr, "db_create: %s\n", db_strerror(ret));
    }

    // Open the database, create it if need be
    if((ret = dbase->open(dbase, NULL, location, NULL, DB_HASH,
        DB_CREATE, 0664)) != 0)
    {
        dbase->err(dbase, ret, "%s", location);
    }
    return dbase;
}

void quiz_loop(DB *deck)
{
    DBC *cursorp;
    DBT key, data;
    int ret,j,n,i,q;
    cardnode *top = NULL;
    cardnode *dummy;
    cardnode *run, *nrun, *mrun;
    int *p;
    card *Cp;
    char c;

    // Set up DBT's
    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));

    // Set up the cursor
    deck->cursor(deck, NULL, &cursorp, 0);

    // Loop through the db and get the relevant info
    while((ret = cursorp->get(cursorp, &key, &data, DB_NEXT)) == 0)
    {
        Cp = data.data;
        if(Cp->date < today)
        {
            dummy = (cardnode*) malloc(sizeof(cardnode));

            p = key.data;
            dummy->C = *Cp;
            dummy->n = *p;

            dummy->next = top;
            dummy->prev = NULL;

            if(top != NULL)
                top->prev = dummy;
        
            top = dummy;
        }
    }

    shuffle_deck(&top);

    run = top;
    q = 0;
    while(run != NULL && !q)
    {
//        mvprintw(20,5,"run->n: %d", run->n);
//        mvprintw(21,5,"run->C.I: %d", run->C.I);
//        mvprintw(22,5,"run->C.date: %d", run->C.date);
//        mvprintw(22,40,"%s", ctime(&run->C.date));
//        mvprintw(23,5,"run->C.EF: %f", run->C.EF);
//        refresh();
//        getch();

        nrun = run->next;
        c = quiz_card(&run->C);
        switch(c)
        {
            case 'c':
                if(run->C.I == 0)
                    delay_card(&run);
                else
                    replace_card(&run->C, run->n);
                break;

            case 'e':
                edit_card(&run->C);
                replace_card(&run->C, run->n);
                delay_card(&run);
                break;

            case 'q':
                q = 1;
                break;
        }
        run = nrun;
    }

    if(cursorp != NULL)
        cursorp->close(cursorp);
    clear_board();
}

void delay_card(cardnode ** CN)
{
    cardnode *run = *CN;
    cardnode *thenext = run->next;
    if(thenext == NULL)
        return;
    

    cardnode *mrun = run;
    int i;

    for(i=0; i<10 && mrun->next != NULL; )
        mrun = mrun->next;
    
    if(run->prev != NULL)
        run->prev->next = run->next;
    if(run->next != NULL)
        run->next->prev = run->prev;
    
    run->prev = mrun;
    run->next = mrun->next;
    
    if(mrun->next != NULL)
        mrun->next->prev = run;
    mrun->next = run;
    *CN = thenext;
}

// return 0 means keep going, return 1 means stop.
char quiz_card(card *C)
{
    int j;
    wchar_t c;
    wchar_t R[MESSAGE_L+1];
    int q = 0;
    int I,ret;


    clear_board();
    refresh();

    // Set up the front- and backside and options using a WINDOW
    WINDOW *fbox;
    fbox = newwin(MESSAGE_H+2, MESSAGE_L+2, 4, 4);
    wattron(fbox,COLOR_PAIR(5));
    box(fbox,0,0);
    wattroff(fbox,COLOR_PAIR(5));
    wrefresh(fbox);

    WINDOW *bbox;
    bbox = newwin(MESSAGE_H+2, MESSAGE_L+2, 4, 44);
    wattron(bbox,COLOR_PAIR(5));
    box(bbox,0,0);
    wattroff(bbox,COLOR_PAIR(5));
    wrefresh(bbox);

    WINDOW *obox;
    obox = newwin(4, 52, MESSAGE_H + 10, 15);
    wrefresh(bbox);


    // Frontside
    for(j=0; j<MESSAGE_H; j++)
    {
        wcsncpy(R, &(C->front[j*MESSAGE_L]), MESSAGE_L);
        mvwaddnwstr(fbox, j+1, 1, R, MESSAGE_L);
    }
    wrefresh(fbox);

    mvwprintw(obox,0,0,"Press any key, or q to quit");
    wrefresh(obox);

    if(getch() == 'q')
        return 'q';

    refresh();
    

    // Backside
    for(j=0; j<MESSAGE_H; j++)
    {
        wcsncpy(R, &(C->back[j*MESSAGE_L]), MESSAGE_L);
        mvwaddnwstr(bbox, j+1, 1, R, MESSAGE_L);
    }
    wrefresh(bbox);

    wclear(obox);

    mvwprintw(obox,0,0,"1) No way!     2) Hard       3) OK         4) Easy");
    mvwprintw(obox,2,0,"e) Edit this card                          q) Quit");
    wrefresh(obox);

    refresh();

    while(!q)
    {
        get_wch(&c);
        switch(c)
        {
            // No way Jose!
            case '1':
                if(C->EF > EF_BASE)
                    C->EF = EF_BASE;
                C->I = 0;
                C->date = today;
                q = 1;
                break;

            // Difficult
            case '2':
                if(C->EF > EF_MIN)
                    C->EF -= 0.1;
            // OK
            case '3':
            // Easy
            case '4':
                if(c == '4' && C->EF < EF_MAX)
                    C->EF += 0.1;

                if(C->I == 0)
                    C->I = DAY;
                else
                    C->I = (int) (C->EF * C->I);

                C->date = (int) today + C->I;
                q = 1;
                break;

            case 'e':
                return 'e';

            case 'q':
                return 'q';
                break;
        }
    }

    delwin(fbox);
    delwin(bbox);

    return 'c';
}

void shuffle_deck(cardnode **Top)
{
    cardnode *run = *Top;
    if(run == NULL || run->next == NULL)
        return;

    while(run != NULL)
    {
        run->C.sort = rand();
        run = run->next;
    }

    b_sort(Top);
}

// Until I'm not too stupid to implement quick sort, I'll leave this here.
void b_sort(cardnode **Top)
{
    cardnode *run = *Top;
    cardnode *runn;
    cardnode *top = *Top;
    int q = 0;
    while(!q)
    {
        q = 1;
        run = top;
        while(run != NULL && run->next != NULL)
        {
            if(run->C.sort > run->next->C.sort)
            {
                runn = run->next;

                run->next = runn->next;
                if(run->next != NULL)
                    run->next->prev = run;
                runn->prev = run->prev;
                if(runn->prev != NULL)
                    runn->prev->next = runn;
                else
                    top = runn;

                runn->next = run;
                run->prev = runn;
                q = 0;
            }
            run = run->next;
        }
    }
    *Top = top;
}

void q_sort(cardnode **Top)
{
    cardnode *fhalf = *Top;
    if(fhalf == NULL || fhalf->next == NULL)
        return;

    int l = 0;
    int m;
    cardnode *shalf = *Top;
    cardnode *shalfdummy;
    cardnode *run = *Top;
    cardnode *dummy;

    while(run != NULL)
    {
        if(l%2)
            shalf = shalf->next;
        l++;
        run = run->next;
    }
    shalfdummy = shalf;
    
    // the middle value
    m = shalf->C.sort;

    // split the list into two (careful ...)
    shalf->prev->next = NULL;
    shalf->prev = NULL;

    run = fhalf;
    while(run != NULL)
    {
        dummy = run->next;
        if(m < run->C.sort)
        {
            if(run->prev == NULL)
                fhalf = fhalf->next;
            else
                run->prev->next = run->next;

            if(run->next != NULL)
                run->next->prev = run->prev;

            run->prev = NULL;
            run->next = shalf;
            shalf->prev = run;
            shalf = run;
        }
        run = dummy;
    }

    run = shalfdummy;
    while(run != NULL)
    {
        dummy = run->next;
        if(run->C.sort < m)
        {
            if(run->prev == NULL)
                shalf = shalf->next;
            else
                run->prev->next = run->next;

            if(run->next != NULL)
                run->next->prev = run->prev;

            run->prev = NULL;
            run->next = fhalf;
            fhalf->prev = run;
            fhalf = run;
        }
        run = dummy;
    }

    q_sort(&fhalf);
    q_sort(&shalf);

    if(fhalf == NULL)
    {
        *Top = shalf;
        return;
    }
    run = fhalf;
    while(run->next != NULL)
        run = run->next;
    
    run->next = shalf;
}

void menu(DB* deck)
{
    char c;
    int i;
    // q is the quit switch
    int q = 0;
    int state = 0;
    char *m_items[] =
    {
        "Flip the cards       ",
        "Add card             ",
        "Quit                 ",
        (char *)NULL,
    };
    int m_len = 3;

    while(!q)
    {
        clear_board();
        for(i=0; i<m_len; i++)
        {
            if(i==state)
            {
                attron(COLOR_PAIR(3));
                mvprintw(7+2*i, 30, "-> ");
                attroff(COLOR_PAIR(3));
            }
            else
                mvprintw(7+2*i, 30, "   ");
            mvprintw(7+2*i, 33, m_items[i]);
        }
        draw_art();
        refresh();
        c = getch();
        switch(c)
        {
            case 'k': // Up!
                state--;
                if(state == -1)
                    state = m_len - 1;
                break;

            case 'j': // Down!
                state++;
                if(state == m_len)
                    state = 0;
                break;

            case '\033':
                getch();
                switch(getch())
                {
                    case 'A': // Up!
                        state--;
                        if(state == -1)
                            state = m_len - 1;
                        break;

                    case 'B': // Down!
                        state++;
                        if(state == m_len)
                            state = 0;
                        break;
                }
                break;
            case '\n':
                switch(state)
                {
                    case 0:
                        quiz_loop(deck);
                        break;
                    case 1:
                        add_card(deck);
                        break;
                    case 2:
                        q = 1;
                        break;
                }
                break;
            case 'q':
                q = 1;
        }
    }
}

void add_card(DB* deck)
{
    DBT key, data;
    int i = max_key(deck) + 1;
    int ret;
    wchar_t s[MESSAGE_P+1];
    
    card C;
    C.date = today+1;
    C.I = 0;
    C.EF = EF_BASE;

    clear_board();
    refresh();

    /* Zero out the DBTs before using them. */
    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));

    key.data = &i;
    key.size = sizeof(int);

    data.data = &C;
    data.size = sizeof(card);

    // Take in some values
    inputbox(5,5,MESSAGE_L,MESSAGE_H, C.front);
    refresh();
    inputbox(5,45,MESSAGE_L,MESSAGE_H, C.back);
    refresh();

    // Add the thing
    ret = deck->put(deck, NULL, &key, &data, DB_NOOVERWRITE);
    if(ret == DB_KEYEXIST)
    {
        deck->err(deck, ret,
            "Put failed because key %d already exists", i);
    }
    deck->sync(deck, 0);
}

int max_key(DB* deck)
{
    DBC *cursorp;
    DBT key, data;
    int ret, n;
    int max = 0;
    int *dummy;

    // Set up DBT's
    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));

    key.data = &n;
    key.size = sizeof(int);

    // Set up the cursor
    deck->cursor(deck, NULL, &cursorp, 0);

    while((ret = cursorp->get(cursorp, &key, &data, DB_NEXT)) == 0)
    {
        dummy = key.data;
        n = *dummy;
        if(n > max)
            max = n;
    }

    if(cursorp != NULL)
        cursorp->close(cursorp);
    
    return max;
}

int count_cards(DB* deck)
{
    DBC *cursorp;
    DBT key, data;
    int ret;
    int n = 0;

    // Set up DBT's
    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));

    key.data = &n;
    key.size = sizeof(int);

    // Set up the cursor
    deck->cursor(deck, NULL, &cursorp, 0);

    while((ret = cursorp->get(cursorp, &key, &data, DB_NEXT)) == 0)
        n++;

    if(cursorp != NULL)
        cursorp->close(cursorp);
    
    return n;
}

void clear_board()
{
    int i;
    for(i=1; i<23; i++)
    {
        mvprintw(i, 0, "                                                                              ");
    }
    draw_frame();
}

void draw_frame()
{
    int i;
    int cards = count_cards(deck);

    char version[40];
    sprintf(version, "version %.1f", VERSION);

    
    attron(COLOR_PAIR(2));
    move(0,0);
    addch(ACS_ULCORNER);

    for(i=1; i<37; i++)
    {
        move(0,i);
        addch(ACS_HLINE);
    }

    move(0,37);
    addch(ACS_RTEE);

    attroff(COLOR_PAIR(2));

    mvprintw(0,38,"flip");

    attron(COLOR_PAIR(2));

    move(0,42);
    addch(ACS_LTEE);

    for(i=43; i<79; i++)
    {
        move(0,i);
        addch(ACS_HLINE);
    }

    move(0,79);
    addch(ACS_URCORNER);


    for(i=1; i<23; i++)
    {
        move(i,0);
        addch(ACS_VLINE);
        move(i,79);
        addch(ACS_VLINE);
    }


    
    move(23,0);
    addch(ACS_LLCORNER);

    move(23,1);
    addch(ACS_RTEE);
    attroff(COLOR_PAIR(2));

    mvprintw(23,2, deck_loc);

    mvprintw(23,3+strlen(deck_loc), "%d", cards);

    attroff(COLOR_PAIR(2));

    move(23,4+strlen(deck_loc) + intlog10(cards));

    attron(COLOR_PAIR(2));
    addch(ACS_LTEE);

    for(i=5+strlen(deck_loc) + intlog10(cards); i<77-strlen(version); i++)
    {
        move(23,i);
        addch(ACS_HLINE);
    }

    move(23,77-strlen(version));
    addch(ACS_RTEE);

    attroff(COLOR_PAIR(2));
    mvprintw(23,78-strlen(version), "%s", version);
    attron(COLOR_PAIR(2));

    move(23,78);
    addch(ACS_LTEE);

    move(23,79);
    addch(ACS_LRCORNER);
    attroff(COLOR_PAIR(2));

    refresh();
}

int intlog10(int n)
{
    int r = 0;
    if(n<0)
        return -1;
    while(n >= 10)
    {
        n /= 10;
        r++;
    }
    return r;
}

void inputbox(int y, int x, int l, int h, wchar_t *s)
{
    WINDOW *ibox;
    ibox = newwin(h+2,l+2, y-1, x-1);
    wattron(ibox,COLOR_PAIR(4));
    box(ibox,0,0);
    wattroff(ibox,COLOR_PAIR(4));
    wrefresh(ibox);

    wchar_t r[l*h+1];
    wchar_t R[l+1];
    int q = 0;
    wchar_t c;
    int i = 0;
    int le = 0;
    int j;

    for(j=0; j<l*h; j++)
        r[j] = (wchar_t)' ';
    r[l*h] = (wchar_t) 0;
    
    move(y, x);
    refresh();
    curs_set(1);

    // i is the location of the pointer
    // l is the length of the input so far
    while((c = (wchar_t)getwc(stdin)) != (wchar_t)'\n')
    {
        switch(c)
        {
            // enter to stop the loop
            case 13:
                q = 1;
                break;

            case 2:
            case KEY_LEFT:
                if(i>0)
                    i--;
                break;

            case 6:
            case KEY_RIGHT:
                if(i<le && i<l*h-1)
                    i++;
                break;

            // backspace to erase one character
            case 127:
                if(i > 0)
                {
                    i--;
                    for(j=i; j<le-1; j++)
                        r[j] = r[j+1];
                    r[le-1] = (wchar_t)' ';
                    le--;
                }
                break;


            // ^u to erase everything
            case 21:
                for(j=0; j<l*h; j++)
                    r[j] = (wchar_t)' ';
                i = 0;
                break;
            case 27:
                getwc(stdin);
                switch(getwc(stdin))
                {
                    // left arrow
                    case 68:
                        if(i>0)
                            i--;
                        break;
                    // right arrow
                    case 67:
                        if(i<le && i<l*h-1)
                            i++;
                        break;
                    // delete
                    case 51:
                        if(le > 0 && i != le)
                        {
                            for(j=i; j<le-1; j++)
                                r[j] = r[j+1];
                            r[le-1] = (wchar_t)' ';
                            le--;
                        }
                        getwc(stdin);
                        break;
                }
                break;

            // enter some characters
            default:
                if(le<l*h)
                {
                    for(j=le; j>i; j--)
                        r[j] = r[j-1];
                    r[i] = c;
                    if(i<l*h-1)
                        i++;
                    le++;
                }

        }

        for(j=0; j<h; j++)
        {
            wcsncpy(R, &(r[j*l]), l);
            mvwaddnwstr(ibox, j+1, 1, R, l);
        }
        wrefresh(ibox);
        move(y + (int) (i/l), x + (i%l));
        refresh();
        if(q)
            break;

    }
    curs_set(0);
    delwin(ibox);
    wcsncpy(s, r, MESSAGE_P);
}

void draw_art()
{
    attron(COLOR_PAIR(5));
    mvprintw(6 ,4,"┌───────────────┐     ");
    mvprintw(7 ,4,"│               │     ");
    mvprintw(8 ,4,"│               │     ");
    mvprintw(9 ,4,"│    ┌──────────┴────┐");
    mvprintw(10,4,"└────┤               │");
    mvprintw(11,4,"     │               │");
    mvprintw(12,4,"┌────┴──────────┐    │");
    mvprintw(13,4,"│               │────┘");
    mvprintw(14,4,"│               │");
    mvprintw(15,4,"│               │");
    mvprintw(16,4,"└───────────────┘");

    mvprintw(6 ,54,"┌───────────────┐     ");
    mvprintw(7 ,54,"│               │     ");
    mvprintw(8 ,54,"│               │     ");
    mvprintw(9 ,54,"│    ┌──────────┴────┐");
    mvprintw(10,54,"└────┤               │");
    mvprintw(11,54,"     │               │");
    mvprintw(12,54,"┌────┴──────────┐    │");
    mvprintw(13,54,"│               │────┘");
    mvprintw(14,54,"│               │");
    mvprintw(15,54,"│               │");
    mvprintw(16,54,"└───────────────┘");
    attroff(COLOR_PAIR(5));

    mvprintw(8 , 7, "snúa við");
    mvprintw(11,12, "flip");
    mvprintw(14, 7, "  برگرداندن");

    mvprintw( 8,57, "întoarce");
    mvprintw(11,62, "voltear");
    mvprintw(14,57, "⠋⠇⠊⠏");
}

void edit_card(card * C)
{
    clear_board();
    refresh();

    draw_side(9+MESSAGE_H,  5, C->front);
    draw_side(9+MESSAGE_H, 45, C->back);

    inputbox(5,5,MESSAGE_L,MESSAGE_H, C->front);
    refresh();
    inputbox(5,45,MESSAGE_L,MESSAGE_H, C->back);
    refresh();
}

void draw_side(int y, int x, wchar_t *side)
{
    int j;
    wchar_t R[MESSAGE_L+1];

    WINDOW *dbox;
    dbox = newwin(MESSAGE_H+2,MESSAGE_L+2, y-1, x-1);
    wattron(dbox,COLOR_PAIR(5));
    box(dbox,0,0);
    wattroff(dbox,COLOR_PAIR(5));

    for(j=0; j<MESSAGE_H; j++)
    {
        wcsncpy(R, &(side[j*MESSAGE_L]), MESSAGE_L);
        mvwaddnwstr(dbox, j+1, 1, R, MESSAGE_L);
    }


    wrefresh(dbox);
    delwin(dbox);
}

void replace_card(card * C, int n)
{
    DBT key, data;
    int ret;

    // Set up DBT's
    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));

    key.data = &n;
    key.size = sizeof(int);

    data.data = C;
    data.size = sizeof(card);
 
    deck->del(deck, NULL, &key, 0);
    deck->sync(deck, 0);
    ret = deck->put(deck, NULL, &key, &data, 0);
}

/*
void clean_database()
{
    char deck_loc_new[209];
    strcpy(deck_loc_new, deck_loc);
    strcat(deck_loc_new, ".clean");
    printf("New one: %s\n", deck_loc_new);
    if(access(deck_loc_new, F_OK) != -1)
    {
        printf("File %s already exists!\n", deck_loc_new);
        return;
    }
    printf("continuing...\n");

    DB* newdeck = init_db(deck_loc_new);
    DBC *cursorp;
    DBT key, data;

    int *p;
    int ret;
    int num = 0;
    card *Cp;
    
    // Set up DBT's
    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));

    // Set up the cursor
    deck->cursor(deck, NULL, &cursorp, 0);

    // Loop through the db and copy everything into the new database
    while((ret = cursorp->get(cursorp, &key, &data, DB_NEXT)) == 0)
    {
        key.data = &num;

        ret = deck->put(newdeck, NULL, &key, &data, DB_NOOVERWRITE);
        if(ret == DB_KEYEXIST)
        {
            deck->err(deck, ret,
                "Put failed because key %d already exists", num);
        }
        else
        {
            num++;
            printf("Put in element %d\n", num);
        }

        deck->sync(deck, 0);
    }
    if(newdeck != NULL)
        newdeck->close(deck,0);
    printf("continuing...\n");
}
*/
